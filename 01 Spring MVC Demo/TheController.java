
package com.pack.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//@author Krishnasai GA

@Controller
public class TheController {
	
	@RequestMapping("/")
	public String showThis() {
		return "main-menu";
	}

}
