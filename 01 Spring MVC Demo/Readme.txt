Dynamic web project
Maven
Dependencies
web.xml
	display name
	servlet
	servlet-mapping
MVCConfig.xml
	component scan to detect controller beans
	enable mvc configuration
	Promote view resolver bean
TheController.java
	Simple class
	Annotate @Controller to show that it's a controller
	a method
	provide mapping for the method
	return view
main-menu.jsp
	this is the view, so place it in "webcontent/web-inf/view" folder
	write simple html structure to display a message

Add a Tomcat server and run on server